﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3.Models;

namespace WebApplication3.Controllers
{
    public class OglasController : Controller
    {
        private AutomobilEntities1 db = new AutomobilEntities1();

        // GET: Oglas
        public ActionResult Index()
        {
            return View(db.Oglas.ToList());
        }

        // GET: Oglas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ogla ogla = db.Oglas.Find(id);
            if (ogla == null)
            {
                return HttpNotFound();
            }
            return View(ogla);
        }




        // GET: Oglas/Create
        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Ogla ogla, HttpPostedFileBase slika)
        {
            if (!ModelState.IsValid)
            {
                return View(ogla);
                
            }
               // ogla.Photo = Url.Content("~/Uploads/" + fileName);
            if (slika!=null)
	{
		 string fileName = Path.GetFileName(slika.FileName);
                
       string path = Path.Combine(Server.MapPath("~/Uploads/"), fileName);
                slika.SaveAs(path);
                ogla.Photo="~/Uploads/"+ fileName;
               
	}


                db.Oglas.Add(ogla);
                db.SaveChanges();
            

            return RedirectToAction("Index");
        }


        //This action gets the photo file for a given Photo ID
 


        //// POST: Oglas/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "id,marka,model,godina,motor,snaga,gorivo,karoserija,opis,cena,kontakt,Photo,AlternateText")] Ogla ogla)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Oglas.Add(ogla);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(ogla);
        //}

        // GET: Oglas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ogla ogla = db.Oglas.Find(id);
            if (ogla == null)
            {
                return HttpNotFound();
            }
            return View(ogla);
        }

        // POST: Oglas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,marka,model,godina,motor,snaga,gorivo,karoserija,opis,cena,kontakt,Photo,AlternateText")] Ogla ogla)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ogla).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(ogla);
        }

        // GET: Oglas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ogla ogla = db.Oglas.Find(id);
            if (ogla == null)
            {
                return HttpNotFound();
            }
            return View(ogla);
        }

        // POST: Oglas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ogla ogla = db.Oglas.Find(id);
            db.Oglas.Remove(ogla);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
